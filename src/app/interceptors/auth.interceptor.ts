import { Injectable } from '@angular/core';
import {HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const key = 'bda53789-d59e-46cd-9bc4-2936630fde39';
    if ( key ) {
      const headers = new HttpHeaders({        
        'x-api-key': key,
      });
      const reqClone = req.clone({
        headers
      });
      return next.handle(reqClone);
    } else {
      return next.handle(req);
    }
  }
}
