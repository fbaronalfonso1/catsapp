import { Component, OnInit } from '@angular/core';
import { CatsService } from '../../services/cats.service';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-cats',
  templateUrl: './cats.page.html',
  styleUrls: ['./cats.page.scss'],
})
export class CatsPage implements OnInit {

  cats: any = []
  constructor(private catsService: CatsService,
              private menuController: MenuController) { }

  ngOnInit() {
    this.showCats();
    this.menuController.enable(false);
    this.menuController.swipeGesture(false);
  }

  showCats(){
    this.catsService.showAll()
    .subscribe((response: any) => {
      this.cats = response
      console.log(this.cats, 'RESPONSE')
    })
  }
}
