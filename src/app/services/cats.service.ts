import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class CatsService {
  API = environment.server;

  constructor(private httpClient: HttpClient) { }

  showAll() {
    return this.httpClient.get(`${ this.API }/`);
  }

}
